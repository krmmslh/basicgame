package fr.karim.game.one.FirstStep;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Trail extends GameObject {

	private float alpha = 1;
	private float life;
	private Handler handler;
	
	private Color color;

	private int width;

	private int height;
	
	public Trail(float x, float y, ID id, float f, int height, int width, Color color,  Handler handler) {
		super(x, y, id);
		this.handler = handler;
		this.color = color;
		this.width = width;
		this.height = height;
		this.life = f;
	}

	@Override
	public void tick() {
		if (life < alpha) {
			alpha -= (life - 0.0001f);
		} else handler.removeObject(this);
	}

	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	private AlphaComposite makeTransparent (float alpha) {
		int type = AlphaComposite.SRC_OVER;
		return AlphaComposite.getInstance(type, alpha);
	}
	
	@Override
	public void render(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setComposite(makeTransparent(alpha));
		
		g.setColor(color);
		g.fillRect((int) x,(int) y, width, height);
		g2d.setComposite(makeTransparent(1));
		
	}
	
	

}
