package fr.karim.game.one.FirstStep;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {

	private Handler handler;
	
	public KeyInput(Handler handler) {
		this.handler = handler;
	}
	
	
	@Override
	public void keyPressed( KeyEvent e) {
		int key = e.getKeyCode();
		
		handler.object.stream().filter(player -> player.getId().equals(ID.PLAYER)).forEach(player -> player.movePlayer(key));
		
		if (key == KeyEvent.VK_ESCAPE) System.exit(1);
	}
	


	@Override
	public void keyReleased( KeyEvent e) {
		int key = e.getKeyCode();
		handler.object.parallelStream().filter(player -> player.getId().equals(ID.PLAYER)).forEach(player -> player.resetMovement(key));

	}
	
	
	
}
