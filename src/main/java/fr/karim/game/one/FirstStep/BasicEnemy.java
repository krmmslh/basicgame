package fr.karim.game.one.FirstStep;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class BasicEnemy extends GameObject {

	private Random seed = new Random();

	private Handler handler;
	
	public BasicEnemy(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		
		velX = 5;
		velY = 5;
		this.handler = handler;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int)x,(int)y, 16, 16);
	}

	@Override
	public void tick() {
		x += velX;
		y += velY;
		
		if (y <=  0  || y >=  Game.HEIGHT-51) {
			velY *= -1;
		}
		if (x <=  0  || x >= Game.WIDTH-16) {
			velX *= -1;
		}
		
		handler.addObject(new Trail(x, y, ID.TRAIL,  0.1f, 16, 16, Color.RED, handler));
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.fillRect((int)x, (int)y, 16, 16);
	}
	
}
