package fr.karim.game.one.FirstStep;

import java.awt.Graphics;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.CopyOnWriteArrayList;

public class Handler {

	
		CopyOnWriteArrayList<GameObject> object = new CopyOnWriteArrayList<>();
		
		
		
		 public void tick() {
			 for(int i = 0; i < object.size(); i++)
				 object.get(i).tick();
		 }
		 
		 public void render (Graphics g) {
			 for(int i = 0; i < object.size(); i++)
				 object.get(i).render(g);
		 }
		 
		 
		 public void addObject(GameObject gameObject) {
			 object.add(gameObject);
		 }
		 
		 public void removeObject(GameObject gameObject) {
			 object.remove(gameObject);
		 }
	
}
