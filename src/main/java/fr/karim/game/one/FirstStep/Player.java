package fr.karim.game.one.FirstStep;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Random;

public class Player extends GameObject{

	private Random seed = new Random();
	private Handler handler;
	
	public Player(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
	}

	@Override
	public void tick() {
		if(!(x<= 0 && velX < 0) &&  !(x >= Game.WIDTH-40 && velX > 0 )) {
			x += velX;
		} 
		
		
		if(!(y<= 0 && velY < 0) &&  !(y >= Game.HEIGHT-67 && velY > 0 )) {
			y += velY;
		} 	
		collision();
	}

	
	public void collision() {
		handler.object.stream().filter(object -> object.getId() == ID.BASICENEMY || object.getId() == ID.FASTENEMY).filter(enemy -> getBounds().intersects(enemy.getBounds())).forEach(enemy -> enemyCollision(enemy));
	}
	
	public void enemyCollision(GameObject enemy) {
		HUD.HEALTH -= 5;
	}
	
	@Override
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillRect((int)x, (int)y, 32, 32);
		
	}
	
	
	public void movePlayer(int key) {
		switch(key) {
		case KeyEvent.VK_UP:
			velY = -5;
			break;
		case KeyEvent.VK_DOWN:
			velY = 5;
			break;
		case KeyEvent.VK_LEFT:
			velX = -5;
			break;
		case KeyEvent.VK_RIGHT:
			velX = 5;
			break;
		}
		
	}
		
	public void resetMovement(int key) {
		switch(key) {
		case KeyEvent.VK_UP:
			velY = 0;
			break;
		case KeyEvent.VK_DOWN:
			velY = 0;
			break;
		case KeyEvent.VK_LEFT:
			velX = 0;
			break;
		case KeyEvent.VK_RIGHT:
			velX = 0;
			break;
		}
				
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, 32,32);
	}

	
}
