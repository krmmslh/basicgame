package fr.karim.game.one.FirstStep;

import java.util.Random;

public class Spawn {

	private Handler handler;
	private HUD hud;
	
	private int scoreKeep = 0;
	
	Random seed = new Random();
	
	public Spawn(Handler handler, HUD hud) {
		this.handler = handler;
		this.hud = hud;
	}
	
	public void tick() {
		scoreKeep++;

		if(scoreKeep % 200 == 0) {
			scoreKeep = 0;
			hud.setLevel(hud.getLevel()+1);
			System.out.println(hud.getLevel());
			if (hud.getLevel() == 5) {
				handler.addObject(new SmartEnemy(seed.nextInt(Game.WIDTH), seed.nextInt(Game.HEIGHT),ID.SMARTENEMY, handler));				
			}
			else if (hud.getLevel()== 4 ) {
				handler.addObject(new FastEnemy(seed.nextInt(Game.WIDTH), seed.nextInt(Game.HEIGHT),ID.FASTENEMY, handler));
			} else if (hud.getLevel() < 4){
				handler.addObject(new BasicEnemy(seed.nextInt(Game.WIDTH), seed.nextInt(Game.HEIGHT),ID.BASICENEMY, handler));
			}
		}
	}
	
}
