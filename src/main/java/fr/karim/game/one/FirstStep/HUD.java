package fr.karim.game.one.FirstStep;

import java.awt.Color;
import java.awt.Graphics;

public class HUD {

	public static float HEALTH = 100;
	
	public int greenValue = 255;
	
	private int score = 0;
	private int level = 1;
	
	
	public void tick() {
		HEALTH = Game.clamp(HEALTH,100, 0);
		greenValue = (int)Game.clamp(greenValue, 255, 0);
		greenValue = (int)HEALTH*2;
		score++;
	}
	
	
	public void render(Graphics g) {
		g.setColor(Color.red);
		g.fillRect(15, 15, 200, 32);

		g.setColor(new Color(75, greenValue, 0));
		g.fillRect(15, 15,(int) HEALTH*2, 32);
		g.setColor(Color.white);
		g.drawRect(15, 15, 200, 32);
		
		g.drawString("Score: " + score, 15, 64);
		g.drawString("Level: " + level, 15, 80);

	}


	public static float getHEALTH() {
		return HEALTH;
	}


	public static void setHEALTH(int hEALTH) {
		HEALTH = hEALTH;
	}


	public int getGreenValue() {
		return greenValue;
	}


	public void setGreenValue(int greenValue) {
		this.greenValue = greenValue;
	}


	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	public int getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}
	
}
