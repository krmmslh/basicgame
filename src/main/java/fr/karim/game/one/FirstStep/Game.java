package fr.karim.game.one.FirstStep;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.Random;

public class Game extends Canvas implements Runnable{
      
		public static int WIDTH = 640;
		public static int HEIGHT = WIDTH * 9/16;
		private boolean running = false;
		private Thread thread;
		private Handler handler;
		private HUD hud;
		private Spawn spawner;
		private Random seed = new Random();
		
		
		public static float clamp(float value, float max, float min) {
			if (value > max) {
				return max;
			} 
			if (value < min) {
				return min;
			}
			return value;
		}
		
		public Game() {
			handler = new Handler();
			this.addKeyListener(new KeyInput(handler));
			new Window(WIDTH, HEIGHT, "First GAME", this);

			hud = new HUD();
			spawner = new Spawn(handler, hud);
			handler.addObject(new Player(WIDTH/2-32,HEIGHT/2-32,ID.PLAYER, handler));

		}
	
	
		public synchronized void start () {
			thread = new Thread(this);
			thread.start();
			running=true;
		}
	
		
		public synchronized void stop () {
			
			try {
				thread.join();
				running = false;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
			
		private void tick() {
			handler.tick();
			hud.tick();
			spawner.tick();
		}
		
		private void render() {
			BufferStrategy  bs = this.getBufferStrategy();
			if ( bs == null ) {
				this.createBufferStrategy(3);
				return;
			}
			
			Graphics g = bs.getDrawGraphics();
			g.setColor(Color.black);
			g.fillRect(0, 0 , WIDTH, HEIGHT);
			
			handler.render(g);
			
			hud.render(g);
			
			g.dispose();
			
			bs.show();
		}
		
		public void run () {
			this.requestFocus();
			long lastTime = System.nanoTime();
			double  amountOfTicks = 60.0;
			double ns = 1_000_000_000 / amountOfTicks; 
			double delta = 0;
			long timer = System.currentTimeMillis();
			int frames = 0;
			while (running) {
				long now = System.nanoTime();
				delta += (now - lastTime) / ns;
				lastTime = now;
				while (delta >= 1) {
					tick();
					delta--;
				}
				if (running) {
					render();
				}
				frames++;
				
				if (System.currentTimeMillis() - timer > 1000) {
					timer += 1000;
					frames = 0;
				}
			}
			stop();
		}
	
		public static void main(String[] args) {
			new Game();
		}
	
}
